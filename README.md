LEM (Lan Event Manager)
=======
Version 0.0.1

This project uses Flask and Peewee.

Contributing to project
-----------------------

See Issues for planned milestones and what needs to be done to reach them. Feel free to add issues or comment on existing issues, your input is welcome.

Contributions of documentation, tests, code or issues are welcome, details of how to setup development environment and suggested workflow will be uploaded soon.
